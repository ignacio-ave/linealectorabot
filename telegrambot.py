
import os
import json
from datetime import datetime
import psutil

import telebot

from funciones import *
from chromadb_funciones import *

from transformers import AutoModelForCausalLM, AutoTokenizer
import torch  # Asegúrate de tener importado torch


# Ruta del directorio donde quieres guardar los archivos
config_file_name = "config.json"

def get_or_ask_for_save_path():
    # Verificar si el archivo de configuración ya existe
    if os.path.exists(config_file_name):
        with open(config_file_name, "r") as config_file:
            config = json.load(config_file)
            # Si SAVE_PATH está en el archivo de configuración, usar ese
            if "SAVE_PATH" in config:
                return config["SAVE_PATH"]
    
    # Si el archivo de configuración no existe o no contiene SAVE_PATH,
    # pedir al usuario que ingrese el SAVE_PATH y guardarlo en el archivo de configuración
    save_path = input("Por favor, ingresa la ruta del directorio donde quieres guardar los archivos: ")
    with open(config_file_name, "w") as config_file:
        json.dump({"SAVE_PATH": save_path}, config_file)
    return save_path

# Usar la función para obtener SAVE_PATH
SAVE_PATH = get_or_ask_for_save_path()
print(f"El directorio para guardar los archivos es: {SAVE_PATH}")

                                                                                                                                                                                                                 
print("Inicio de la descarga del modelo y configuración de la API. Por favor, espera...")

# Cargar el modelo y el tokenizador
model_name = "mistralai/Mistral-7b-v0.1"
model = AutoModelForCausalLM.from_pretrained(model_name)
tokenizer = AutoTokenizer.from_pretrained(model_name)

# Verificar si CUDA está disponible y mover el modelo a CUDA si es así
if torch.cuda.is_available():
    model = model.to("cuda")

embeddings, client = load_core()

API_TOKEN = "7158634385:AAGHwuzXljhT9uYEoxPEAwxwq39WkZjR_8o"
bot = telebot.TeleBot(API_TOKEN)

# Mantener un registro de los usuarios que han iniciado el proceso de subida de archivos
users_awaiting_files = {}
users_open_collections = {}
query_results = {}  # Diccionario para almacenar los resultados de las consultas de los usuarios


def format_query_summary(user_id, results):
    output = "🔍 *Resultados de la Consulta:*\n"
    for i, result in enumerate(results['documents']):
        # Asegúrate de que todos los elementos que se van a incluir en el mensaje sean cadenas
        document_id = str(results['ids'][i])
        distance = str(results['distances'][i])
        # Asume que `result` ya es una cadena; de lo contrario, conviértelo adecuadamente
        output += f"{i+1}. ID: {document_id}, Distancia: {distance}\n"
    output += "\nUsa /texto <número del documento> para ver el contenido."
    return output


def escape_markdown_v2(text):
    escape_chars = '_*[]()~`>#+-=|{}.!'
    return ''.join(['\\' + char if char in escape_chars else char for char in text])

def split_message(text, max_length=4096):
    """Divide un texto largo en partes que no superen el límite máximo de longitud."""
    parts = []
    while len(text) > 0:
        if len(text) > max_length:
            part = text[:max_length]
            first_newline = part.rfind('\n')
            if first_newline != -1:
                parts.append(part[:first_newline])
                text = text[first_newline:]
            else:
                parts.append(part)
                text = text[max_length:]
        else:
            parts.append(text)
            break
    return parts

print(r"""   
                                                                                                                                                                                                                 
88               88              88                   88           88                                       88                                                                   88888888ba     ,ad8888ba,  888888888888  
88               ""              ""                   88           ""                                       88                                    ,d                             88      "8b   d8"'    `"8b      88       
88                                                    88                                                    88                                    88                             88      ,8P  d8'        `8b     88       
88  8b,dPPYba,   88   ,adPPYba,  88   ,adPPYba,       88           88  8b,dPPYba,    ,adPPYba,  ,adPPYYba,  88           ,adPPYba,   ,adPPYba,  MM88MMM  ,adPPYba,   8b,dPPYba,  88aaaaaa8P'  88          88     88       
88  88P'   `"8a  88  a8"     ""  88  a8"     "8a      88           88  88P'   `"8a  a8P_____88  ""     `Y8  88          a8P_____88  a8"     ""    88    a8"     "8a  88P'   "Y8  88""""""8b,  88          88     88       
88  88       88  88  8b          88  8b       d8      88           88  88       88  8PP"""""""  ,adPPPPP88  88          8PP"""""""  8b            88    8b       d8  88          88      `8b  Y8,        ,8P     88       
88  88       88  88  "8a,   ,aa  88  "8a,   ,a8"      88           88  88       88  "8b,   ,aa  88,    ,88  88          "8b,   ,aa  "8a,   ,aa    88,   "8a,   ,a8"  88          88      a8P   Y8a.    .a8P      88       
88  88       88  88   `"Ybbd8"'  88   `"YbbdP"'       88888888888  88  88       88   `"Ybbd8"'  `"8bbdP"Y8  88888888888  `"Ybbd8"'   `"Ybbd8"'    "Y888  `"YbbdP"'   88          88888888P"     `"Y8888Y"'       88       
                                                                                                                                                                                                                    
                                                                                                                                                                                                                    
""") 

@bot.message_handler(commands=['start'])
def send_welcome(message):
    welcome_text = (
        "🎓📚 Bienvenido al Bot Académico y Profesional 📚🎓\n\n"
        
        "Este bot está diseñado para ayudarte a gestionar y explorar documentos académicos y profesionales de una manera eficiente y efectiva. Aquí puedes:\n\n"
        
        "- 📖 Subir documentos en formato EPUB para su procesamiento.\n"
        "- 🛠 Procesar y añadir los documentos a tu colección personalizada.\n"
        "- 🔍 Realizar búsquedas dentro de las colecciones para encontrar información relevante.\n"
        "- 📚 Abrir y explorar distintas colecciones de documentos.\n\n"
        
        "Para comenzar, puedes subir un archivo usando el comando `/update` o ver los libros ya añadidos con `/abrir`. Si necesitas ayuda o quieres conocer más sobre las funcionalidades disponibles, utiliza el comando `/help`.\n\n"
        
        "Si estás listo para explorar y descubrir, ¡vamos a empezar! 🚀"
    )
    bot.reply_to(message, welcome_text, parse_mode='Markdown')


@bot.message_handler(commands=['help'])
def send_help(message):
    help_text = (
        "🤖 *Guía de Uso del Bot Académico* 🤖\n\n"
        
        "Facilitando la interacción con una base de datos documental, este bot ofrece herramientas para la gestión eficiente de documentos académicos. A continuación, se presentan los comandos para navegar y utilizar sus capacidades:\n\n"
        
        "🚀 *Comandos Esenciales:*\n"
        "- */start*: Despliega un mensaje de bienvenida e instrucciones iniciales.\n"
        "- */help*: Proporciona detalles sobre los comandos disponibles.\n"
        "- */tutorial*: Ofrece un tutorial paso a paso para familiarizarse con las funcionalidades del bot.\n"
        "- */status*: Reporta el estado operativo actual del bot.\n"
        "- */info*: Presenta una descripción general del proyecto y su propósito.\n\n"
        
        "📁 *Gestión Documental:*\n"
        "- */update*: Prepara el bot para recibir un archivo EPUB para su inclusión en la base de datos.\n"
        "- */procesar*: Inicia el procesamiento de archivos EPUB previamente subidos, incorporándolos a la base de datos.\n\n"
        
        "🔍 *Exploración y Búsqueda:*\n"
        "- */abrir*: Permite seleccionar una colección específica para realizar búsquedas dentro de ella.\n"
        "- */query*: Ejecuta una búsqueda de texto dentro de la colección activa, facilitando la recuperación de documentos relevantes.\n\n"
        
        "⚙️ *Mantenimiento (Futuro Desarrollo):*\n"
        "- */eliminar*: Este comando, actualmente en desarrollo, permitirá eliminar documentos específicos de la base de datos.\n\n"
        
        "Para consultas adicionales o asistencia, por favor, utilice el comando /info. Su exploración del conocimiento comienza aquí."
    )
    bot.reply_to(message, help_text, parse_mode='Markdown')


@bot.message_handler(commands=['tutorial'])
def send_tutorial(message):
    tutorial_text = (
        "📚 **Guía Rápida para el Bot Académico** 📚\n\n"
        
        "Aprovecha al máximo este bot para gestionar y explorar documentos académicos en formato EPUB. Sigue esta guía breve para comenzar:\n\n"
        
        "1️⃣ **Subir Archivo:**\n"
        "- Comando: `/update`\n"
        "- Acción: Sube un archivo EPUB siguiendo las indicaciones.\n\n"
        
        "2️⃣ **Procesamiento:**\n"
        "- Comando: `/procesar`\n"
        "- Acción: El bot procesará y añadirá el archivo a la base de datos.\n\n"
        
        "3️⃣ **Abrir Colección:**\n"
        "- Comando: `/abrir`\n"
        "- Acción: Selecciona una colección para buscar documentos relevantes.\n\n"
        
        "4️⃣ **Consulta:**\n"
        "- Comando: `/query <consulta>`\n"
        "- Acción: Realiza búsquedas textuales dentro de la colección abierta.\n\n"
        
        "🔍 **Ejemplo:**\n"
        "Para buscar información sobre el teorema de Pitágoras: `/query teorema de pitágoras`.\n\n"
        
        "Utiliza `/help` para obtener una descripción detallada de todos los comandos. Estamos aquí para facilitar tu acceso al conocimiento académico. ¡Empecemos!"
    )
    bot.reply_to(message, tutorial_text, parse_mode='Markdown')


@bot.message_handler(commands=['info'])
def send_info(message):
    info_text = (
        "🎓 *Descripción del Proyecto* 🎓\n\n"
        
        "Este proyecto integra técnicas de procesamiento de lenguaje natural y modelos de lenguajes de grandes dimensiones para analizar contenidos textuales académicos. Mediante el uso de bases de datos no convencionales, se habilita la indexación semántica y la recuperación eficiente de documentos a partir de consultas textuales en colecciones de formato EPUB.\n\n"
        
        "🔍 *Funcionalidades:*\n"
        "- Transformación de textos académicos en representaciones vectoriales.\n"
        "- Búsqueda por similitud para identificar documentos relevantes mediante consultas de texto.\n"
        "- Interfaz basada en comandos para facilitar el acceso y la exploración de la base de datos.\n\n"
        
        "El proyecto destaca por su aplicación de PLN y LLMs en la mejora de la gestión del conocimiento académico, permitiendo una exploración precisa del contenido. Para colaboraciones o más información, por favor contactar.\n\n"
        "Avanzando en la búsqueda y gestión de conocimiento académico."
    )
    bot.reply_to(message, info_text, parse_mode='Markdown')



@bot.message_handler(commands=['status'])
def send_status(message):
    # Obtener el uso de recursos
    cpu_percent = psutil.cpu_percent(interval=1)  # Porcentaje de uso de CPU en el último segundo
    memory_percent = psutil.virtual_memory().percent  # Porcentaje de uso de memoria de tipo virtual
    
    client_heartbeat = client.heartbeat()
    # Aquí podrías incluir lógica para verificar el estado real del bot o de sus componentes.
    status_text = (
        "🚀 *Estado del Bot* 🚀\n\n"
        
        "El bot está actualmente *funcionando correctamente*.\n\n"
        
        "✅ *Última Verificación:* " + f"`{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}`" + "\n"  + f"📊 *Uso de Recursos:* `CPU: {cpu_percent}%, Memoria: {memory_percent}%`" + "\n"+ f"🔗 *Estado del Cliente:* `Conectado`" if client_heartbeat else "`Desconectado`" 
        
        "Si experimentas algún problema o tienes preguntas, utiliza el comando `/help` para obtener asistencia."
    )
    bot.reply_to(message, status_text, parse_mode='Markdown')



@bot.message_handler(commands=['update'])
def request_file(message):
    users_awaiting_files[message.from_user.id] = True
    reply_text = (
        "📁 *Subida de Archivos* 📁\n\n"
        "Por favor, sube el archivo (EPUB, PDF o TXT) (Actualmente solo EPUB) que deseas añadir a tu colección. 📚\n\n"
        "⚠️ *Atención:* Actualmente solo aceptamos archivos en formato *EPUB*. Asegúrate de que tu archivo cumpla con este formato antes de enviarlo.\n\n"
        "Después de subir el archivo, usa el comando `/procesar` para incluirlo en la base de datos y empezar a explorar su contenido."
    )
    bot.reply_to(message, reply_text, parse_mode='Markdown')


@bot.message_handler(content_types=['document'])
def handle_docs(message):
    user_id = message.from_user.id
    if user_id in users_awaiting_files and users_awaiting_files[user_id]:
        try:
            file_info = bot.get_file(message.document.file_id)
            downloaded_file = bot.download_file(file_info.file_path)

            os.makedirs(SAVE_PATH, exist_ok=True)
            file_path = os.path.join(SAVE_PATH, message.document.file_name)

            with open(file_path, 'wb') as new_file:
                new_file.write(downloaded_file)

            success_reply = (
                "🎉 *Archivo Guardado con Éxito* 🎉\n\n"
                "Tu archivo ha sido guardado exitosamente en nuestra base de datos. 📖\n\n"
                "Ahora puedes proceder a procesarlo con el comando `/procesar` para añadirlo a tu colección y realizar búsquedas dentro de su contenido."
            )
            bot.reply_to(message, success_reply, parse_mode='Markdown')
            users_awaiting_files[user_id] = False  # Resetear el estado
        except Exception as e:
            error_reply = f"❌ Ocurrió un error al guardar el archivo: {e}\n\nPor favor, intenta nuevamente o contacta con el soporte si el problema persiste."
            bot.reply_to(message, error_reply, parse_mode='Markdown')
            users_awaiting_files[user_id] = False  # Resetear el estado en caso de error
    else:
        reminder_reply = "ℹ️ Por favor, usa primero el comando `/update` antes de enviar el archivo."
        bot.reply_to(message, reminder_reply, parse_mode='Markdown')


@bot.message_handler(commands=['procesar'])
def procesar_epub(message):
    bot.reply_to(message, "🔄 Procesando archivos... Esto puede tomar unos momentos.", parse_mode='Markdown')
    
    # Simular un proceso de procesamiento de archivos
    epub_files = identify_epub_files()
    resultado = process_and_upload_book(epub_files, client, embeddings)
    
    if resultado:
        success_message = (
            "✅ *Procesamiento Completado*\n\n"
            "Los archivos han sido procesados y añadidos a la base de datos exitosamente. Ahora puedes usar el comando `/abrir` para explorar tus colecciones."
        )
        bot.reply_to(message, success_message, parse_mode='Markdown')
    else:
        bot.reply_to(message, "❌ Ocurrió un error durante el procesamiento. Puede que no hayan libros por procesar. Por favor, intenta nuevamente.", parse_mode='Markdown')


@bot.message_handler(commands=['abrir'])
def abrir_coleccion(message):
    args = message.text.split()[1:]  # Extraer argumentos del comando
    if args:
        nombre_coleccion = ' '.join(args)
        # Verificar si la colección especificada existe
        colecciones_disponibles = read_documentation_csv(unique_collections=True)
        if nombre_coleccion in colecciones_disponibles:
            # Guardar la colección abierta para este usuario
            users_open_collections[message.from_user.id] = nombre_coleccion
            bot.reply_to(message, f"Colección '{nombre_coleccion}' abierta.")
        else:
            bot.reply_to(message, "La colección especificada no existe.")
    else:
        # Listar todas las colecciones disponibles
        colecciones_disponibles = read_documentation_csv(unique_collections=True)
        respuesta = "Colecciones disponibles:\n" + "\n".join(colecciones_disponibles)
        bot.reply_to(message, respuesta)


@bot.message_handler(commands=['query'])
def realizar_query(message):
    user_id = message.from_user.id
    args = message.text.split()[1:]
    if not args:
        bot.reply_to(message, "Por favor, especifica el texto a buscar.")
        return
    
    texto_a_buscar = ' '.join(args)
    if user_id not in users_open_collections or not users_open_collections[user_id]:
        bot.reply_to(message, "Primero debes abrir una colección con el comando /abrir.")
        return
    
    nombre_coleccion = users_open_collections[user_id]
    collection = get_database(client, nombre_coleccion, embeddings)
    try:
        query_embedding = create_embedding(embeddings, [texto_a_buscar])[0]
        results = query(collection, query_embeddings=[query_embedding], n_results=5)
        if results['documents']:
            # Almacenar solo los IDs de los documentos en query_results
            query_results[user_id] = results['ids'][0]  # Asumiendo que 'ids' es una lista de listas como se observó
            # Generar y enviar el resumen de la consulta
            respuesta = "🔍 *Resultados de la Consulta:*\n"
            for i, doc_id in enumerate(results['ids'][0]):
                respuesta += f"{i+1}. Documento ID: {doc_id}, Distancia: {results['distances'][0][i]:.4f}\n"
            respuesta += "\nUsa /texto <número> para ver el contenido del documento."
            bot.reply_to(message, respuesta, parse_mode='Markdown')
        else:
            bot.reply_to(message, "No se encontraron resultados.")
    except Exception as e:
        bot.reply_to(message, f"Error al realizar la consulta: {e}")


@bot.message_handler(commands=['texto'])
def mostrar_texto_documento(message):
    user_id = message.from_user.id
    args = message.text.split()[1:]
    if not args or not args[0].isdigit():
        bot.reply_to(message, "Por favor, especifica el número del documento que deseas ver.", parse_mode='Markdown')
        return
    
    numero_documento = int(args[0]) - 1
    if user_id not in query_results or numero_documento < 0 or numero_documento >= len(query_results[user_id]):
        bot.reply_to(message, "Número de documento inválido o fuera de rango.", parse_mode='Markdown')
        return
    
    doc_id = query_results[user_id][numero_documento]
    collection_name = users_open_collections[user_id]
    collection = get_database(client, collection_name, embeddings)
    documento = get_documents(collection, ids=[doc_id])
    
    if documento is not Exception:
        # Parte 1: Enviar mensaje inicial con detalles del documento usando Markdown
        mensaje_inicial = f"📄 *Contenido del Documento ID {doc_id}*"
        bot.reply_to(message, mensaje_inicial, parse_mode='Markdown')

        # Parte 2: Enviar el texto del documento sin formato Markdown
        # Asegúrate de que el texto del documento no tenga caracteres que interfieran con Markdown.
        texto_documento = documento['documents'][0]
        bot.send_message(message.chat.id, texto_documento)

    else: 
        bot.reply_to(message, "Error al obtener el documento.", parse_mode='Markdown')


@bot.message_handler(commands=['preguntar'])
def realizar_pregunta(message):
    user_id = message.from_user.id
    texto_usuario = message.text.split(maxsplit=1)[1] if len(message.text.split(maxsplit=1)) > 1 else "No especificaste una pregunta."

    if user_id not in users_open_collections or not users_open_collections[user_id]:
        bot.reply_to(message, "Primero debes abrir una colección con el comando /abrir.")
        return
    
    nombre_coleccion = users_open_collections[user_id]
    collection = get_database(client, nombre_coleccion, embeddings)
    
    try:
        # Realizar la búsqueda en la base de datos como en la función `realizar_query`
        query_embedding = create_embedding(embeddings, [texto_usuario])[0]
        results = query(collection, query_embeddings=[query_embedding], n_results=5)
        
        # Preparar el prompt incluyendo resultados de la consulta para generar una respuesta con el modelo Mistral
        if results['documents']:
            documentos_relacionados = ', '.join([f"Documento {i+1}" for i in range(len(results['ids'][0]))])
            prompt = f"Basado en la pregunta: '{texto_usuario}' y considerando los documentos relevantes encontrados ({documentos_relacionados}), ¿cuál sería una respuesta adecuada?"
            prompt = prompt[:tokenizer.model_max_length]  # Asegurarse de no exceder el límite de tokens
            
            # Generar la respuesta del modelo
            inputs = tokenizer(prompt, return_tensors="pt", max_length=512, truncation=True)
            inputs = inputs.to("cuda")
            output_sequences = model.generate(
                input_ids=inputs["input_ids"],
                attention_mask=inputs["attention_mask"],
                max_length=256,
                num_return_sequences=1,
                no_repeat_ngram_size=2,
                temperature=0.7,
                do_sample=True  
            )

            respuesta = tokenizer.decode(output_sequences[0], skip_special_tokens=True)
            
            bot.reply_to(message, respuesta)
        else:
            bot.reply_to(message, "No se encontraron resultados relevantes para generar una respuesta.")
    except Exception as e:
        bot.reply_to(message, f"Error al generar la respuesta: {e}")
        
        



#procesar epub subidos
#abrir coleccion
#preguntar a pdf 

@bot.message_handler(func=lambda message: True)
def echo_all(message):
    bot.reply_to(message, "No entiendo tu mensaje, por favor usa un comando o pregúntame algo. Si necesitas ayuda usa /help")


    
bot.polling()
