# LineaLectoraBOT

Bot de Telegram para gestionar archivos PDF o TXT. Procesa y almacena el contenido en una base de datos de vectores tokenizados, facilitando la búsqueda de conceptos y citas académicas. El usuario puede hacer preguntas, las cuales son comparadas con los vectores para encontrar párrafos con alta similaridad. Ideal para investigadores y estudiantes que buscan extraer citas o localizar información relevante de forma rápida y eficiente.

## Funcionalidades:
- **Subida de archivos**: PDF/TXT.
- **Procesamiento**: Tokenización y almacenamiento en base de datos vectorial.
- **Búsqueda**: Consulta mediante preguntas en lenguaje natural.
- **Resultados**: Retorno de párrafos con mayor relevancia.

## Requisitos:
- Telegram.
- Archivos en formato PDF/TXT.
