# Codigo realizado por: Ignacio Astorga V.

# Este archivo contiene las funciones que se utilizan para interactuar con una colección de ChromaDB 
# Estas funciones estan escritas de manera modular, para que puedan ser utilizadas en cualquier entorno
# y para que puedan ser reutilizadas en otros proyectos.
# Tambien el propósito de estas funciones es que sean lo mas simples posibles, para que sean faciles de entender.


import chromadb
from chromadb.utils import embedding_functions
import subprocess

""" 
oooooooooooo                                    o8o                                           
`888'     `8                                    `"'                                           
 888         oooo  oooo  ooo. .oo.    .ooooo.  oooo   .ooooo.  ooo. .oo.    .ooooo.   .oooo.o 
 888oooo8    `888  `888  `888P"Y88b  d88' `"Y8 `888  d88' `88b `888P"Y88b  d88' `88b d88(  "8 
 888    "     888   888   888   888  888        888  888   888  888   888  888ooo888 `"Y88b.  
 888          888   888   888   888  888   .o8  888  888   888  888   888  888    .o o.  )88b 
o888o         `V88V"V8P' o888o o888o `Y8bod8P' o888o `Y8bod8P' o888o o888o `Y8bod8P' 8""888P' 
                                                                                              
create_embedding_function                                                                                              
create_embedding                                                                                              
create_in_memory_client
create_server_http_client
create_database
get_database
create_or_get_database
add_documents_to_collection
update_documents
upsert_documents
delete_documents
get_documents
query
delete_database

"""

# Create_embedding_function 
# Funcion que crea una funcion de embeddings, se le puede pasar el nombre de la funcion de embeddings
# por ejemplo "default" o "sentence_transformer"
# retorna una funcion de embeddings

def create_embedding_function(name):
    if name == "default":
        return embedding_functions.DefaultEmbeddingFunction()
    elif name == "sentence_transformer":
        return embedding_functions.SentenceTransformerEmbeddingFunction(model_name="all-MiniLM-L6-v2")
    else:
        raise ValueError("Invalid embedding function")
    
# Create_embedding
# Funcion que crea un embedding
# Se le pasa un string y una funcion de embeddings
# retorna un embedding



def create_embedding(embedding_callable, texts):
    
    if not isinstance(texts, list) or not all(isinstance(text, str) for text in texts):
        raise ValueError("texts must be a list of strings")
    
    embedding = embedding_callable(texts)  
    return embedding


# Create_client
# Funcion que crea un cliente para la base de datos
# Se le pasa el path de la base de datos
# retorna un cliente

def create_in_memory_client(path):
    client = chromadb.PersistentClient(path=path)
    return client

def create_server_http_client(path):
    subprocess.run(["chroma", "run", "--path", path])
    client = chromadb.HttpClient(host='localhost', port=8000)
    return client

# Solo uno de estos puede ser ejecutado.





# Create_database a Create_or_get_database
# Funcion que crea una base de datos
# Se le pasa un cliente, el nombre de la base de datos y una funcion de embeddings
# retorna una coleccion


def create_database(client, name, embedding_function):
    return client.create_collection(name=name, embedding_function=embedding_function)
    
def get_database(client, name, embedding_function):
    return client.get_collection(name=name, embedding_function=embedding_function)
    
def create_or_get_database(client, name, embedding_function):
    return client.get_or_create_collection(name=name, embedding_function=embedding_function)


# Add_documents_to_collection
# Funcion que añade documentos a una coleccion
# Se le pasa una coleccion y los documentos, embeddings, metadatas, ids
# retorna None


def add_documents_to_collection(collection, documents=None, embeddings=None, metadatas=None, ids=None):
    collection.add(documents=documents, embeddings=embeddings, metadatas=metadatas, ids=ids)

# Update_documents
# Funcion que actualiza documentos en una coleccion
# Se le pasa una coleccion, los ids, documentos, embeddings, metadatas
# retorna None
    

def update_documents(collection, ids, documents=None, embeddings=None, metadatas=None):
    collection.update(ids=ids, documents=documents, embeddings=embeddings, metadatas=metadatas)

# Upsert_documents
# Funcion que actualiza o inserta documentos en una coleccion
# Se le pasa una coleccion, los ids, documentos, embeddings, metadatas
# retorna None

    
def upsert_documents(collection, ids, documents=None, embeddings=None, metadatas=None):
    collection.upsert(ids=ids, documents=documents, embeddings=embeddings, metadatas=metadatas)

# Delete_documents
# Funcion que elimina documentos en una coleccion
# Se le pasa una coleccion, los ids, y opcionalmente un where
# retorna None
    
def delete_documents(collection, ids, where=None):
    collection.delete(ids=ids, where=where)

# Get_documents
# Funcion que retorna documentos en una coleccion
# Se le pasa una coleccion, los ids, y opcionalmente un where y un include
# retorna una lista de documentos

    
def get_documents(collection, ids=None):
    return collection.get(ids=ids)

def get_documents_where(collection, ids=None, where=None):
    return collection.get(ids=ids, where=where)



# Query
# Funcion que realiza una query en una coleccion
# Se le pasa una coleccion, los query_embeddings, query_texts, n_results, y opcionalmente un where, where_document, include
# retorna una lista de documentos

def query(collection, query_embeddings=None, query_texts=None, n_results=10, where=None, where_document=None):
    return collection.query(query_embeddings=query_embeddings, query_texts=query_texts, n_results=n_results, where=where, where_document=where_document)

# Delete_database
# Funcion que elimina una coleccion
# Se le pasa un cliente y el nombre de la coleccion
# retorna None

def delete_database(client, name):
    client.delete_collection(name=name)
    



    """
    
    
ooooooooooooo                        .   
8'   888   `8                      .o8   
     888       .ooooo.   .oooo.o .o888oo 
     888      d88' `88b d88(  "8   888   
     888      888ooo888 `"Y88b.    888   
     888      888    .o o.  )88b   888 . 
    o888o     `Y8bod8P' 8""888P'   "888" 
                                        
    
    
def print_status_message(message):
    border = "#" * (len(message) + 6)  # Aumentamos el tamaño del borde para mejorar la presentación
    print(f"{'#' * (len(border) - 2)}")
    print(f"#{' ' * (len(border) - 4)}#")  # Línea vacía antes del mensaje
    print(f"#  {message}  #")  # Añadimos espacios adicionales para centrar el mensaje
    print(f"#{' ' * (len(border) - 4)}#")  # Línea vacía después del mensaje
    print(f"{'#' * (len(border) - 2)}")


print_status_message("Creando funcion de embeddings")
sentence_transformer = create_embedding_function("sentence_transformer")

print_status_message("Creando cliente")
cliente = create_in_memory_client("datos")

print_status_message("Creando base de datos")
datos_collection = create_database(cliente, "datos", sentence_transformer)

print("Añadiendo documentos a la coleccion")

print_status_message("Añadiendo documentos a la colección")
add_documents_to_collection(
    datos_collection,
    documents=["primer documento", "segundo documento", "tercer documento"],
    embeddings=[
        create_embedding(sentence_transformer, "primer documento")[0],  # Ajuste aquí
        create_embedding(sentence_transformer, "segundo documento")[0],  # Ajuste aquí
        create_embedding(sentence_transformer, "tercer documento")[0]    # Ajuste aquí
    ],
    metadatas=[
        {"numero": 1, "palabras": 2},
        {"numero": 2, "palabras": 2},
        {"numero": 3, "palabras": 2}
    ],
    ids=["primer_id", "segundo_id", "tercer_id"]
)

# add_documents_to_collection(datos_collection, documents=["primer documento", "segundo documento", "tercer documento"], embeddings


print("Obteniendo documentos de la coleccion")
print(get_documents(datos_collection, ids=["primer_id", "segundo_id", "tercer_id"]))

print("Query en la coleccion")
print(query(datos_collection, query_texts=["primer documento"], n_results=3))

print("Actualizando documentos de la coleccion")
update_documents(datos_collection, ids=["primer_id", "segundo_id", "tercer_id"], documents=["primer documento actualizado", "segundo documento actualizado", "tercer documento actualizado"])

print("Eliminando documentos de la coleccion")
delete_documents(datos_collection, ids=["primer_id", "segundo_id", "tercer_id"], where={"palabras": 2})

print("Eliminando la coleccion")
delete_database(cliente, "datos")


      
    """