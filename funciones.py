# %% 
"""
# crear nuevas funciones

subir pdf
abrir coleccion
preguntar a pdf 



create_embedding_function                                                                                              
create_embedding                                                                                              
create_in_memory_client
create_server_http_client
create_database
get_database
create_or_get_database
add_documents_to_collection
update_documents
upsert_documents
delete_documents
get_documents
query
delete_database
"""


import os
import zipfile
import shutil
from bs4 import BeautifulSoup
from chromadb_funciones import *
import chromadb
from chromadb.utils import embedding_functions
import csv


def process_epub(epub_path, output_folder='base de datos/temporal', cleanup=True):
    
    """
    Procesa un archivo EPUB extrayendo su contenido y el texto de los archivos HTML.
    Opcionalmente, limpia la carpeta de salida después de extraer el texto.

    Args:
    epub_path (str): Ruta al archivo EPUB.
    output_folder (str): Carpeta de salida para los archivos descomprimidos.
    cleanup (bool): Si se debe limpiar la carpeta de salida después de la extracción. Por defecto es True.

    Returns:
    list: Lista de listas con el texto de cada página.
    """
    
    try:
        # Crear la carpeta de salida si no existe
        os.makedirs(output_folder, exist_ok=True)

        # Descomprimir el archivo EPUB
        with zipfile.ZipFile(epub_path, 'r') as zip_ref:
            zip_ref.extractall(output_folder)

        # Extraer el texto de los archivos HTML descomprimidos
        pages_text = []  # Lista que contendrá el texto de cada página
        for root, dirs, files in os.walk(output_folder):
            for file in sorted(files):
                if file.endswith('.html'):
                    file_path = os.path.join(root, file)
                    with open(file_path, 'r', encoding='utf-8') as f:
                        content = f.read()
                        soup = BeautifulSoup(content, 'html.parser')
                        paragraphs = [p.get_text() for p in soup.find_all('p')]
                        if paragraphs:
                            pages_text.append(paragraphs)

        # Opcional: Limpiar la carpeta de salida
        if cleanup:
            for root, dirs, files in os.walk(output_folder, topdown=False):
                for name in files:
                    os.remove(os.path.join(root, name))
                for name in dirs:
                    shutil.rmtree(os.path.join(root, name))

        return pages_text
    except Exception as e:
        print(f"Ocurrió un error: {e}")
        return []
    
    
"""
# Uso de la función
epub_path = 'base de datos/Calculus - Michael Spivak - 3ra Edición(1).epub'
output_folder = '/run/media/ig/Documentos/PORTAFOLIO/python/LíneaLectoraBot/base de datos/temporal'
epub_text = process_epub(epub_path, output_folder)

# Muestra el resultado
if epub_text:
    print(f"Se extrajo el texto de {len(epub_text)} páginas.")
    for i, page in enumerate(epub_text[:5], start=1):
        print(f"Página {i}:")
        for para in page:
            print(para)
        print("\n---\n")
else:
    print("No se pudo extraer el texto del archivo EPUB.")

"""


from chromadb_funciones import *
# Function to identify .epub files in a folder

def identify_epub_files(folder_path='base de datos/textos'):
    epub_files = []  # List to store the .epub files
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            if file.endswith('.epub'):
                epub_files.append(os.path.join(root, file))
    return epub_files


def remove_epub_extension(document_names):
    modified_names = []  # List to store the modified names
    for name in document_names:
        modified_names.append(name.replace('.epub', ''))  # Remove ".epub" from each name
    return modified_names


def extract_document_names(epub_files):
    document_names = []
    for file in epub_files:
        document_name = os.path.basename(file)
        document_names.append(document_name)
    modified_names = remove_epub_extension(document_names)
    
    return modified_names


def validate_collection_name(name):
    """
    Validates a collection name based on the specified rules:
    1. Contains 3-63 characters
    2. Starts and ends with an alphanumeric character
    3. Contains only alphanumeric characters, underscores, or hyphens (-)
    4. Contains no two consecutive periods (..)
    5. Is not a valid IPv4 address
    """
    import re
    import ipaddress

    # Rule 1: Length check
    if not (3 <= len(name) <= 63):
        return False

    # Rule 2: Start and end with an alphanumeric character
    if not (name[0].isalnum() and name[-1].isalnum()):
        return False

    # Rule 3: Contains only alphanumeric, underscores, or hyphens
    if not re.match(r'^[a-zA-Z0-9_-]+$', name):
        return False

    # Rule 4: No two consecutive periods
    if '..' in name:
        return False

    # Rule 5: Not a valid IPv4 address
    try:
        ipaddress.ip_address(name)
        # If conversion is successful, it's a valid IP address
        return False
    except ValueError:
        # If conversion fails, it's not a valid IP address
        return True


def format_query_results(results):
    output = "Resultados de la consulta:\n"
    for i, result in enumerate(results['documents']):
        output += f"Documento {i+1}:\n"
        output += f"ID: {results['ids'][i]}\n"
        output += f"Distancia: {results['distances'][i]}\n"
        if 'metadatas' in results and results['metadatas'][i]:
            output += "Metadatos: " + str(results['metadatas'][i]) + "\n"
        output += f"Texto: {result}\n"
        output += "---\n"
    return output






def transform_and_validate_name(original_name):
    """
    Transforms a collection name to meet the specified rules and validates it:
    1. Contains 3-63 characters
    2. Starts and ends with an alphanumeric character
    3. Contains only alphanumeric characters, underscores, or hyphens (-)
    4. Contains no two consecutive periods (..)
    5. Is not a valid IPv4 address
    
    The transformation will:
    - Replace spaces and other disallowed characters with underscores
    - Trim the name to meet the length requirement if necessary
    - Ensure the name starts and ends with an alphanumeric character
    """
    import re
    import ipaddress

    # Replace spaces and invalid characters with underscores
    transformed_name = re.sub(r'[^a-zA-Z0-9_-]', '_', original_name)
    
    # Trim the name to the maximum allowed length if necessary, ensuring it ends with an alphanumeric character
    if len(transformed_name) > 63:
        transformed_name = transformed_name[:63].rstrip('_')
        # Ensure it ends with an alphanumeric character by trimming any trailing non-alphanumeric characters
        transformed_name = re.sub(r'_+$', '', transformed_name)
    
    # If the name is too short after trimming, append underscores to meet the minimum length
    if len(transformed_name) < 3:
        transformed_name += '_' * (3 - len(transformed_name))

    # Check if the transformed name is valid
    is_valid = validate_collection_name(transformed_name)
    
    return transformed_name, is_valid


def load_core(file_path='base de datos/vectores', embedding_function="sentence_transformer"):
    # Cargar sentence_transformers embeddings
    sentence_transformer = create_embedding_function(embedding_function)
    cliente = create_in_memory_client(file_path)    
    return sentence_transformer, cliente


def flatten_embedding(embedding):
    if isinstance(embedding, list) and len(embedding) > 0 and isinstance(embedding[0], list):
        # Assuming embedding is a list of lists and you want to flatten it to a single list
        flat_embedding = [item for sublist in embedding for item in sublist]
        return flat_embedding
    return embedding  # Return the embedding directly if it's already flat


def update_documentation_csv(collection_name, file_name, number_of_pages):
    csv_file_path = "documentos_añadidos.csv"
    # Comprobar si el archivo existe para escribir la cabecera solo si es necesario
    file_exists = os.path.isfile(csv_file_path)
    
    with open(csv_file_path, mode='a', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        # Si el archivo no existe, escribimos la cabecera
        if not file_exists:
            writer.writerow(["Nombre de la Colección", "Nombre del Archivo", "Número de Páginas"])
        writer.writerow([collection_name, file_name, number_of_pages])

def read_documentation_csv(unique_collections=False):
    csv_file_path = "documentos_añadidos.csv"
    collection_file_list = []
    unique_collection_names = set()

    try:
        with open(csv_file_path, mode='r', encoding='utf-8') as file:
            reader = csv.reader(file)
            next(reader, None)  # Saltar la cabecera
            for row in reader:
                if row:
                    collection_name = row[0]
                    file_name = row[1]
                    collection_file_list.append((collection_name, file_name))
                    unique_collection_names.add(collection_name)
    except FileNotFoundError:
        print("El archivo 'documentos_añadidos.csv' no existe.")
    except Exception as e:
        print(f"Ocurrió un error al leer el archivo: {e}")

    if unique_collections:
        return list(unique_collection_names)
    else:
        return collection_file_list


        
def process_and_upload_book(epub_files, client, embedding_function):
    output_folder = 'base de datos/temporal'
    
    # Leer la lista de documentos ya procesados
    processed_books = read_documentation_csv()  # Asumiendo que esta función devuelve una lista de tuplas (nombre de la colección, nombre del archivo)

    for file_path in epub_files:
        book_name = os.path.splitext(os.path.basename(file_path))[0]
        transformed_name, is_valid = transform_and_validate_name(book_name)  # Transformar y validar el nombre

        # Verificar si el libro ya ha sido procesado
        if any(transformed_name == collection_name and book_name == file_name for collection_name, file_name in processed_books):
            print(f"Libro '{transformed_name}' ya procesado. Omitiendo.")
            continue

        print(f"Procesando {file_path}")
        epub_text = process_epub(file_path, output_folder)  # Procesar el archivo EPUB

        if not is_valid:
            print(f"Nombre de libro '{book_name}' inválido después de la transformación.")
            continue

        book_collection = create_or_get_database(client, transformed_name, embedding_function)

        for page_number, page_text in enumerate(epub_text, start=1):
            document_id = f"{transformed_name}_page_{page_number}"
            # Asegurarse de que page_text es una cadena o una lista de cadenas
            if isinstance(page_text, list):
                # Si page_text es una lista, convertirla a una sola cadena de texto
                page_text_combined = ' '.join(page_text)
            else:
                page_text_combined = page_text  # Si ya es una cadena, se usa directamente
            # Generar el embedding
            page_embedding = create_embedding(embedding_function, [page_text_combined])
            page_embedding_flat = flatten_embedding(page_embedding)
            metadata = {"page_number": page_number}
            upsert_documents(book_collection, ids=[document_id], documents=[page_text_combined], embeddings=[page_embedding_flat], metadatas=[metadata])

        # Actualizar el CSV con los detalles del libro procesado
        update_documentation_csv(transformed_name, book_name, len(epub_text))

        print(f"Libro '{transformed_name}' procesado y subido correctamente.")


# %%
# Ejemplo de uso
"""
epub_files = identify_epub_files()
embeddings, client = load_core()
process_and_upload_book(epub_files, client, embeddings)


# %%


# Primero, obtenemos la colección en la que queremos buscar
collection_name = "CalculoUnaVariable"  # Ajusta el nombre de la colección según sea necesario
collection = get_database(client, collection_name, embeddings)

# Generamos el embedding para la consulta usando el texto deseado
query_text = "Integración por partes"
query_embedding = create_embedding(embeddings, [query_text])[0]

# Realizamos la consulta a la colección
print(f"Realizando consulta en la colección {collection_name}")
results = query(collection, query_embeddings=[query_embedding], n_results=5)



print_text = format_query_results(results)

print(print_text)

"""
